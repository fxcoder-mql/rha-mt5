# Индикатор RHA (Recursive Heiken Ashi)

Рекурсивный (вложенный) Heiken Ashi с несколькими стилями баров.

![](media/20200417_062644_RHAx13.png)

## Параметры

- **Depth**: степень вложенности:
	- Original price: исходная цена
	- Classic HA (x1): классический HA, степень вложенности 1
	- HA(HA(...)) (x#): HA по HA с указанной вложенностью
- **Bull Color**: цвет бычьей свечи (вверх)
- **Bear Color**: цвет медвежьей свечи (вниз)
- **Bar Style**: стиль бара:
	- Candle (with shaded tail): свеча (с затенёнными хвостами)
	- Narrow bar (with shaded tail): узкий бар (с затенёнными хвостами)
	- Wide bar (with shaded tail): широкий бар (с затенёнными хвостами)
	- Narrow body: только узкое тело
	- Wide body: только широкое тело
- **Shading Percent**: процент затенения хвостов для стилей с хвостами (0% - минимальная видимость, 100% - максимальная)
- **Color Only**: только расцветка, в этом режиме значения бара будут равны оригинальным ценам, а цвет \- тому типу цены или HA, который укажите в **Depth**

## См. также

В блоге: <https://www.fxcoder.ru/search/label/%7BRHA%7D>

Установка: <https://www.fxcoder.ru/p/install-script.html>

Поддержать: <https://www.fxcoder.ru/donate>

Telegram-канал: <https://t.me/fxcoder_ru>
