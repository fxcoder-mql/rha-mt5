/*
Copyright 2023 FXcoder

This file is part of RHA.

RHA is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RHA is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with RHA. If not, see
http://www.gnu.org/licenses/.
*/

// Chart. © FXcoder

#define _CHART_GET(N, T, P) T       N() const  { return (T)get(P); }
#define _CHART_SET(N, T, P) CChart *N(T value) { return set(P, value); }

#define _CHART_WINDOW_GET(N, T, P) T       N(int window = 0) const  { return (T)get(P, window); }
#define _CHART_WINDOW_SET(N, T, P) CChart *N(int window, T value)   { return set(P, window, value); }

class CChart
{
private:

	long id_; // 0 - current

	// Chart's changing parameters. See check_change().
	double chart_price_min_;
	double chart_price_max_;
	int chart_width_in_bars_;
	int chart_first_visible_bar_;
	int chart_height_in_pixels_;
	int chart_width_in_pixels_;
	datetime chart_zero_bar_time_;


public:

	// Default constructor
	void CChart():
		id_(0),
		chart_price_min_(0),
		chart_price_max_(0),
		chart_width_in_bars_(0),
		chart_first_visible_bar_(0),
		chart_height_in_pixels_(0),
		chart_width_in_pixels_(0),
		chart_zero_bar_time_(0)
	{
	}

	// Constructor for a specific chart Id.
	void CChart(long chart_id):
		id_(chart_id),
		chart_price_min_(0),
		chart_price_max_(0),
		chart_width_in_bars_(0),
		chart_first_visible_bar_(0),
		chart_height_in_pixels_(0),
		chart_width_in_pixels_(0),
		chart_zero_bar_time_(0)
	{
	}

	int objects_delete_all(int subwindow = -1, int object_type = -1) const
	{
		return ObjectsDeleteAll(id_, subwindow, object_type);
	}

	int objects_delete_all(const string prefix, int subwindow = -1, int object_type = -1) const
	{
		return ObjectsDeleteAll(id_, prefix, subwindow, object_type);
	}

	void redraw() { ::ChartRedraw(id_); }

	// Chart background color
	_CHART_GET(color_background, color, CHART_COLOR_BACKGROUND)
	_CHART_SET(color_background, color, CHART_COLOR_BACKGROUND)

	// Chart foreground color
	_CHART_GET(color_foreground, color, CHART_COLOR_FOREGROUND)
	_CHART_SET(color_foreground, color, CHART_COLOR_FOREGROUND)

	// Color for the down bar, shadows and body borders of bear candlesticks
	_CHART_GET(color_chart_down, color, CHART_COLOR_CHART_DOWN)
	_CHART_SET(color_chart_down, color, CHART_COLOR_CHART_DOWN)

	// Color for the up bar, shadows and body borders of bull candlesticks
	_CHART_GET(color_chart_up, color, CHART_COLOR_CHART_UP)
	_CHART_SET(color_chart_up, color, CHART_COLOR_CHART_UP)

	// Text of a comment in a chart
	_CHART_GET(comment, string, CHART_COMMENT)
	_CHART_SET(comment, string, CHART_COMMENT)

	// Send a notification of an event of object deletion (CHARTEVENT_OBJECT_DELETE) to all mql5-programs on a chart
	_CHART_GET(event_object_delete, bool, CHART_EVENT_OBJECT_DELETE)
	_CHART_SET(event_object_delete, bool, CHART_EVENT_OBJECT_DELETE)

	// Number of the first visible bar in the chart. Indexing of bars is the same as for timeseries (r/o)
	_CHART_GET(first_visible_bar, int, CHART_FIRST_VISIBLE_BAR)

	// Price chart in the foreground
	_CHART_GET(foreground, bool, CHART_FOREGROUND)
	_CHART_SET(foreground, bool, CHART_FOREGROUND)

	// Chart height in pixels
	_CHART_WINDOW_GET(height_in_pixels, int, CHART_HEIGHT_IN_PIXELS)

	//
	_CHART_GET(is_maximized, int, CHART_IS_MAXIMIZED)

	//
	_CHART_GET(is_minimized, int, CHART_IS_MINIMIZED)

	// Chart maximum (r/o)
	_CHART_WINDOW_GET(price_max, double, CHART_PRICE_MAX)
	// Chart minimum (r/o)
	_CHART_WINDOW_GET(price_min, double, CHART_PRICE_MIN)

	// Scale (0..5)
	_CHART_GET(scale, int, CHART_SCALE)
	_CHART_SET(scale, int, CHART_SCALE)

	// The number of bars on the chart that can be displayed (r/o)
	_CHART_GET(visible_bars, int, CHART_VISIBLE_BARS)

	// Chart width in bars (r/o)
	_CHART_GET(width_in_bars, int, CHART_WIDTH_IN_BARS)

	// Chart width in pixels (r/o)
	_CHART_GET(width_in_pixels, int, CHART_WIDTH_IN_PIXELS)

	int leftmost_visible_bar() const { return first_visible_bar(); }

	// Самый правый видимый бар, может быть отрицательным, если справа есть отступ.
	// forbid_negative: вернуть 0, если номер бара отрицательный
	int rightmost_visible_bar(bool forbid_negative) const
	{
		int bar = first_visible_bar() - width_in_bars() + 1;

		if (forbid_negative && (bar < 0))
			bar = 0;

		return bar;
	}

	// Толщина бара как линии (не пиксели).
	int bar_body_line_width()
	{
		int scale = scale();

		// see fxcoder/mki#55
		switch (scale)
		{
			case 0: return 1;
			case 1: return 1;
			case 2: return 2;
			case 3: return 3;
			case 4: return 6;
			case 5: return 13;
		}

		//_debug.warning("Unknown bar scale:" + VAR(scale));
		return 1;
	}

	// Ширина бара (включая зазор) в пикселях. Или шаг баров в пикселях.
	int bar_step() const
	{
		return 1 << scale();
	}

	bool check_change(bool check_size, bool check_price, bool check_new_bar, int window = 0)
	{
		bool bad_data = false;
		bool need_update = false;


		if (check_size)
		{
			int chart_width_in_bars = width_in_bars();
			int chart_first_visible_bar = first_visible_bar();
			int chart_height_in_pixels = height_in_pixels(window);
			int chart_width_in_pixels = width_in_pixels();

			bad_data = bad_data ||
				(chart_width_in_bars == 0) || (chart_first_visible_bar == 0) ||
				(chart_height_in_pixels == 0) || (chart_width_in_pixels == 0);

			need_update = need_update ||
				(chart_width_in_bars != chart_width_in_bars_) || (chart_first_visible_bar != chart_first_visible_bar_) ||
				(chart_height_in_pixels != chart_height_in_pixels_) || (chart_width_in_pixels != chart_width_in_pixels_);

			if (!bad_data)
			{
				chart_width_in_bars_ = chart_width_in_bars;
				chart_first_visible_bar_ = chart_first_visible_bar;
				chart_height_in_pixels_ = chart_height_in_pixels;
				chart_width_in_pixels_ = chart_width_in_pixels;
			}
		}

		if (check_price)
		{
			double chart_price_max = price_max(window);
			double chart_price_min = price_min(window);

			bad_data = bad_data ||
				(chart_price_max == 0) || (chart_price_min == 0);

			need_update = need_update ||
				(chart_price_max != chart_price_max_) || (chart_price_min != chart_price_min_);


			if (!bad_data)
			{
				chart_price_max_ = chart_price_max;
				chart_price_min_ = chart_price_min;
			}
		}

		if (check_new_bar)
		{
			datetime time = (datetime)SymbolInfoInteger(ChartSymbol(id_), SYMBOL_TIME);
			datetime chart_zero_bar_time = time - (time % PeriodSeconds(_Period));

			bad_data = bad_data ||
				(chart_zero_bar_time == 0);

			need_update = need_update ||
				(chart_zero_bar_time != chart_zero_bar_time_);

			if (!bad_data)
				chart_zero_bar_time_ = chart_zero_bar_time;
		}

		return need_update && !bad_data;
	}

private:

	CChart *set(ENUM_CHART_PROPERTY_DOUBLE  property_id, double value) { ChartSetDouble( id_, property_id, value); return &this; }
	CChart *set(ENUM_CHART_PROPERTY_INTEGER property_id, long   value) { ChartSetInteger(id_, property_id, value); return &this; }
	CChart *set(ENUM_CHART_PROPERTY_STRING  property_id, string value) { ChartSetString( id_, property_id, value); return &this; }
	CChart *set(ENUM_CHART_PROPERTY_INTEGER property_id, int window, long value) { ChartSetInteger(id_, property_id, value); return &this; }

	double get(ENUM_CHART_PROPERTY_DOUBLE  property_id, int window = 0) const { return ChartGetDouble( id_, property_id, window); }
	long   get(ENUM_CHART_PROPERTY_INTEGER property_id, int window = 0) const { return ChartGetInteger(id_, property_id, window); }
	string get(ENUM_CHART_PROPERTY_STRING  property_id                ) const { return ChartGetString( id_, property_id        ); }
};

CChart _chart;
