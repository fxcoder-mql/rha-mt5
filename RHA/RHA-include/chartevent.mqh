/*
Copyright 2023 FXcoder

This file is part of RHA.

RHA is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RHA is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with RHA. If not, see
http://www.gnu.org/licenses/.
*/

// OnChartEvent helper. © FXcoder

#include "math.mqh"

class CChartEvent
{
private:

	int    id_;
	long   lparam_;
	double dparam_;
	string sparam_;

	// время предыдущего клика в мс для определения двойного клика
	long            chart_dbl_click_prev_click_time_msc_;
	string          chart_dbl_click_prev_symbol_;
	ENUM_TIMEFRAMES chart_dbl_click_prev_period_;


public:

	long   lparam() const { return lparam_; }
	double dparam() const { return dparam_; }
	string sparam() const { return sparam_; }

	void update(const int id, const long &lparam, const double &dparam, const string &sparam)
	{
		id_ = id;
		lparam_ = lparam;
		dparam_ = dparam;
		sparam_ = sparam;
	}
	bool is_chart_change_event() { return id_ == CHARTEVENT_CHART_CHANGE; }

	bool is_custom_event(int event_n)
	{
		const int check_id = CHARTEVENT_CUSTOM + event_n;
		return (check_id == id_) && _math.is_in(check_id, (int)CHARTEVENT_CUSTOM, (int)CHARTEVENT_CUSTOM_LAST + 1);
	}

	// подразумеваются все действия, изменяющие координаты, включая создание и удаление объекта
	bool is_object_move_event()
	{
		return
			(id_ == CHARTEVENT_OBJECT_CREATE) ||
			(id_ == CHARTEVENT_OBJECT_CHANGE) ||
			(id_ == CHARTEVENT_OBJECT_DRAG)   ||
			(id_ == CHARTEVENT_OBJECT_DELETE);
	}

	// подразумеваются все действия, изменяющие координаты, включая создание и удаление объекта
	bool is_object_move_event(string name)
	{
		return is_object_move_event() && (sparam_ == name);
	}

	bool is_key_down_event()
	{
		return id_ == CHARTEVENT_KEYDOWN;
	}

	bool is_key_down_event(ushort key)
	{
		return is_key_down_event() && (key == lparam_);
	}
private:

} _chartevent;
