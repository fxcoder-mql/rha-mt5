/*
Copyright 2023 FXcoder

This file is part of RHA.

RHA is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RHA is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with RHA. If not, see
http://www.gnu.org/licenses/.
*/

// © FXcoder

/*
Число буферов:
	- MT5: 3
*/

#include "colorhistogram2.mqh"

class CBicolorHistogram2: public CColorHistogram2
{
protected:

	color bull_color_;
	color bear_color_;

public:

	void CBicolorHistogram2(int plot_index_first, int buffer_index_first):
		CColorHistogram2(plot_index_first, buffer_index_first, 2),
		bull_color_(clrNONE),
		bear_color_(clrNONE)
	{
	}

	void CBicolorHistogram2(const CPlot &prev):
		CColorHistogram2(prev, 2),
		bull_color_(clrNONE),
		bear_color_(clrNONE)
	{
	}

	virtual CBicolorHistogram2 *init(int plot_index_first, int buffer_index_first) override
	{
		CColorHistogram2::init(plot_index_first, buffer_index_first, 2);
		return &this;
	}

	// true if changed
	bool set_colors(color bull_color, color bear_color)
	{
		validate_two_colors(bull_color, bear_color);

		if ((bull_color == bull_color_) && (bear_color == bear_color_))
			return false;

		bull_color_ = bull_color;
		bear_color_ = bear_color;

		color colors[] { bull_color, bear_color};
		CColorHistogram2::set_colors(colors);
		return true;
	}
};
