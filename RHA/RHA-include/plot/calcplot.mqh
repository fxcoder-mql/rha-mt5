/*
Copyright 2023 FXcoder

This file is part of RHA.

RHA is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RHA is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with RHA. If not, see
http://www.gnu.org/licenses/.
*/

// Класс буфера для вычислений (DRAW_NONE, INDICATOR_CALCULATIONS). © FXcoder

#include "singleplot.mqh"

class CCalcPlot: public CSinglePlot
{
public:

	void CCalcPlot(): CSinglePlot() { }

	void CCalcPlot(int plot_index_first, int buffer_index_first): CSinglePlot()
	{
		init(plot_index_first, buffer_index_first);
	}

	void CCalcPlot(const CPlot &prev): CSinglePlot()
	{
		init(prev);
	}

	virtual CCalcPlot *init(int plot_index_first, int buffer_index_first) override
	{
		init_single(plot_index_first, buffer_index_first, DRAW_NONE);
		return &this;
	}

	virtual CCalcPlot *init(const CPlot &prev) override
	{
		init_single(prev.plot_index_next(), prev.buffer_index_next(), DRAW_NONE);
		return &this;
	}
};
