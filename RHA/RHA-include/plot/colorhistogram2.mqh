/*
Copyright 2023 FXcoder

This file is part of RHA.

RHA is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RHA is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with RHA. If not, see
http://www.gnu.org/licenses/.
*/

// Класс цветной гистограммы на двух индикаторных буферах (DRAW_COLOR_HISTOGRAM2) с имитацией в 4. © FXcoder

/*
Число буферов:
	- MT5: 3
*/

#include "plot.mqh"

class CColorHistogram2: public CPlot
{
protected:

	double buffer_high_[];
	double buffer_low_[];
	double buffer_color_[];

public:

	void CColorHistogram2(int plot_index_first, int buffer_index_first, int colors_count):
		CPlot()
	{
		init(plot_index_first, buffer_index_first, colors_count);
	}

	void CColorHistogram2(const CPlot &prev, int colors_count):
		CPlot()
	{
		init(prev.plot_index_next(), prev.buffer_index_next(), colors_count);
	}

	// по умолчанию 2 цвета
	virtual CColorHistogram2 *init(int plot_index_first, int buffer_index_first) override
	{
		init(plot_index_first, buffer_index_first, 2);
		return &this;
	}

	// по умолчанию 2 цвета
	virtual CColorHistogram2 *init(const CPlot &prev) override
	{
		init(prev.plot_index_next(), prev.buffer_index_next(), 2);
		return &this;
	}

	// Размер буфера (все одинаковой длины, любой подойдёт)
	virtual int size() const override
	{
		return ArraySize(buffer_color_);
	}

	// bars - число последних баров для очистки, -1 = все
	virtual CColorHistogram2 *empty(int bars = -1) override
	{
		double empty_value = empty_value();

		if (bars < 0)
		{
			ArrayInitialize(buffer_high_,  empty_value);
			ArrayInitialize(buffer_low_,  empty_value);
			ArrayInitialize(buffer_color_, 0);
		}
		else
		{
			// empty может быть вызван до инициализации буферов и первого тика
			int size = size();

			if (bars > size)
				bars = size;

			int offset = size - bars;

			ArrayFill(buffer_high_,  offset, bars, empty_value);
			ArrayFill(buffer_low_,  offset, bars, empty_value);
			ArrayFill(buffer_color_, offset, bars, 0);
		}

		return &this;
	}

	virtual CColorHistogram2 *empty_bar(int bar) override
	{
		double empty_value = empty_value();

		buffer_high_       [bar] = empty_value;
		buffer_low_      [bar] = empty_value;
		buffer_color_ [bar] = 0;

		return &this;
	}

	void set_colors(color &colors[])
	{
		if (ArraySize(colors) != color_indexes_)
			return;

		for (int i = 0; i < color_indexes_; i++)
			line_color(i, colors[i]);
	}

	// return true if width changes
	bool update_auto_width()
	{
		static int width_prev = -1;

		int width = _chart.bar_body_line_width();
		if (width == width_prev)
			return false;

		PlotIndexSetInteger(plot_index_first_, PLOT_LINE_WIDTH, width);
		return true;
	}

	void get_values(int bar, double &value1, double &value2)
	{
		value1 = buffer_high_[bar];
		value2 = buffer_low_[bar];
	}

	void set_values(int bar, double high, double low, int color_index)
	{
		buffer_color_[bar] = color_index;
		buffer_high_[bar] = high;
		buffer_low_[bar] = low;
	}

protected:

	virtual CColorHistogram2 *init(int plot_index_first, int buffer_index_first, int colors_count)
	{
		// do not reinitialize

		plot_index_first_ = plot_index_first;
		plot_index_count_ = 1;

		buffer_index_first_ = buffer_index_first;
		buffer_index_count_ = 3;

		SetIndexBuffer(buffer_index_first_, buffer_high_, INDICATOR_DATA);
		SetIndexBuffer(buffer_index_first_ + 1, buffer_low_, INDICATOR_DATA);
		SetIndexBuffer(buffer_index_first_ + 2, buffer_color_, INDICATOR_COLOR_INDEX);

		draw_type(DRAW_COLOR_HISTOGRAM2); // после SetIndexBuffer

		color_indexes(colors_count);
		return &this;
	}
};
